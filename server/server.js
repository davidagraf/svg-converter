var require = __meteor_bootstrap__.require,
    Future      = require('fibers/future'),
    path        = require('path'),
    fs          = require('fs'),
    base        = path.resolve('.');

if (base == '/'){
  base = path.dirname(global.require.main.filename);   
}

function custom_require(name) {
  var modpath = 'node_modules/' + name;
  var publicPath = path.resolve(base+'/public/' + modpath);
  var staticPath = path.resolve(base+'/bundle/static/' + modpath);
  if (fs.existsSync(publicPath)){
    return require(publicPath);
  }
  else if (fs.existsSync(staticPath)){
    return require(staticPath);
  }
  else{
    console.log('node module ' + name + ' not found');
  }
}

var imagemagick = custom_require('imagemagick'),
    xmldom      = custom_require('xmldom'),
    schemaEnv   = custom_require('schema')('myEnvironment', { locale: 'en' })

var querySchema = schemaEnv.Schema.create({
    type: 'object',
    properties: {
        format: {
            type: 'string',
            enum: ['png', 'jpg', 'jpeg', 'gif'],
            optional : true
        },
        width: {
            type: 'integer',
            minimum : 0,
            maximum : 10000,
            optional : true
        },
        height: {
            type: 'integer',
            minimum : 0,
            maximum : 10000,
            optional : true
        },
        ratio: {
            type: 'number',
            minimum : 0.0,
            maximum : 10.0,
            optional : true
        }
    },
    additionalProperties: false
});

Meteor.publish("images", function() {
  return Images.find({owner: this.userId});
});

Meteor.methods({
  saveSVG: function(filename, url) {
    if (!this.userId)
      throw new Meteor.Error(403, "You must be logged in");

    var getResult = Meteor.http.get(url);
    if (getResult.statusCode == 200) {
      var svg = getResult.content;
      // delete filepicker file
      Meteor.http.del(url, {}, function(){});

      return Images.insert({
        'owner': this.userId,
        'name' : filename,
        'svg' : svg,
        'uuid' : Meteor.uuid()
      });
    }
  }
});

Meteor.startup(function() {
  Meteor.Router.add('/img/:uuid', function(uuid) {
    var request = this.request;
    var response = this.response;
    var fut = new Future();

    var mongoDoc = Images.findOne({'uuid' : uuid});

    if (mongoDoc === undefined) {
      response.writeHead(204, {});
      return;
    }

    var svg = mongoDoc.svg;

    var query = request.query;

    var validatedQuery = querySchema.validate(query);
    if (validatedQuery.isError()) {
      response.writeHead(400, {});
      response.write('Query validation failed : '
                   + JSON.stringify(validatedQuery.getError()));
      response.end();
      return;
    }

    var format = 'png';
    if('format' in query) {
      format = query.format;
    }

    var XMLSerializer = xmldom.XMLSerializer;
    var svgDoc = new xmldom.DOMParser().parseFromString(svg);
    var svgRoot = svgDoc.documentElement;
    var svgWidth = parseInt(svgRoot.getAttribute('width'));
    var svgHeight = parseInt(svgRoot.getAttribute('height'));

    var resizeCmd = [];

    if (!isNaN(svgWidth) && !isNaN(svgHeight) && svgRoot.hasAttribute('viewBox')) {
      if ('width' in query) {
        var queryWidth = parseInt(query.width);
        if (!isNaN(queryWidth)) {
          svgHeight = parseInt(svgHeight / svgWidth * queryWidth);
          svgWidth = queryWidth;
        }
      }
      else if ('height' in query) {
        var queryHeight = parseInt(query.height);
        if (!isNaN(queryHeight)) {
          svgWidth = parseInt(svgWidth / svgHeight * queryHeight);
          svgHeight = queryHeight;
        }
      }
      else if ('ratio' in query) {
        var queryRatio = parseFloat(query.ratio);
        if (!isNaN(queryRatio)) {
          svgWidth = parseInt(svgWidth * queryRatio); 
          svgHeight = parseInt(svgHeight * queryRatio); 
        }
      }

      svgRoot.setAttribute('width', svgWidth + "px");
      svgRoot.setAttribute('height', svgHeight + "px");
      svg = new XMLSerializer().serializeToString(svgDoc);
    }
    else {
      resizeCmd = ['-resize'];
      if ('width' in query) {
        resizeCmd = resizeCmd.concat(query.width + 'x');
      }
      else if ('height' in query) {
        resizeCmd = resizeCmd.concat('x' + query.height);
      }
      else if ('ratio' in query) {
        resizeCmd = resizeCmd.concat(parseInt(100 * parseFloat(query.ratio)).toString() + '%');
      }
      else {
        resizeCmd = [];
      }
    }

    response.writeHead(200, {'Content-Type':'image/' + format});

    var convertParams = ['svg:-'].concat(resizeCmd, format + ':-');
    var convert = imagemagick.convert(convertParams);

    convert.on('data', function(data) {
      response.write(data, 'binary');
    });

    convert.on('end', function() {
      response.end();
      fut.ret();
    });

    convert.stdin.write(svg);
    convert.stdin.end();
        
    fut.wait();
  });
});
