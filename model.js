Images = new Meteor.Collection("images");

if (Meteor.isServer) {
  Meteor.startup(function () {
    // TODO: Wait for meteor to support _ensureIndex officially in its api.
    //       _ensureIndex is a temp solution only.
    Images._ensureIndex({"uuid" : 1}, {"unique" : true});
  });
}

Images.allow({
  remove: function (userId, images) {
    return ! _.any(images, function(image) {
      return image.owner !== userId;
    });
  }
});
