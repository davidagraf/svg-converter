Meteor.subscribe("images");

Template.input.events({
  'click button': function(ev) {
     filepicker.pickAndStore(
       {},
       {location:"S3"},
       function(fpfiles){
         $.each(fpfiles, function(index, fpfile) {
           if (fpfile.mimetype == "image/svg+xml") {
             Meteor.call('saveSVG', fpfile.filename, fpfile.url);
           }
         });
       }
    ); 
  }
});

Template.svg_item.events({
  'click .trash' : function(ev) {
    Images.remove({"uuid" : this.uuid});
  }
});

Template.svgs.svgs = function() {
  return Images.find({},{}); 
};

Template.svg_item.origin = document.location.origin;
Template.svg_item_modal.origin = document.location.origin;
Template.svg_item_modal.examples = [ "format=png",
                                    "format=png&width=100",
                                    "format=jpg&height=50",
                                    "format=gif&ratio=1.5"];
